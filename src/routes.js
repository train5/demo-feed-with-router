import HomeBody from './components/body/HomeBody.vue';
import AboutBody from './components/body/AboutBody.vue';
import DetailHome from './components/body/home/DetailHome.vue';

export const routes = [{
        path: '/',
        name: 'home',
        component: HomeBody,
        props: true
    },
    {
        path: '/about',
        component: AboutBody,
        props: true
    },
    {
        path: '/detail/:feedId',
        name: 'detail',
        component: DetailHome,
        props: true
    },
    {
        path: '/feed_author/:authorId',
        name: 'feed_author',
        component: HomeBody,
        props: true
    }
]