import Vue from "vue";
import App from "./App.vue";
import {
  BootstrapVue,
  IconsPlugin
} from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import {
  routes
} from './routes'
import VueRouter from 'vue-router';
import Paginate from "vuejs-paginate";


Vue.config.productionTip = false;
// Install BootstrapVue
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);
// Install Router vue
Vue.use(VueRouter);
Vue.component("paginate", Paginate);

const router = new VueRouter({
  routes: routes,
  mode: "history"
})

new Vue({
  render: (h) => h(App),
  router,
}).$mount("#app");